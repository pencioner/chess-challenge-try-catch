"""Simple console-based terminal interface."""

import sys

from solution import solve
from bitboard import King, Queen, Rook, Bishop, Knight


PIECES = [
    King,
    Queen,
    Rook,
    Bishop,
    Knight
]


def safe_input_int(prompt, default=0):
    """Print prompt and input the integer number.

    In case of empty input return default
    In case of wrong input try again
    """
    result = None
    while result is None:
        try:
            val = input(prompt)
            result = default if val == '' else int(val)
        except KeyboardInterrupt:
            print("\nInterrupted by user...\n")
            sys.exit()
        except ValueError:
            print("You entered wrong value, please try again")
    return result


def user_input():
    """Ask user to provide board size and quantity of each piece."""
    piece_set = {}

    rows = safe_input_int("Enter number of board rows: ")
    cols = safe_input_int("Enter number of board columns: ")

    for klass in PIECES:
        piece_set[klass] = safe_input_int(
            "Enter number of {}s: ".format(klass.full_name()))

    return rows, cols, piece_set


def pretty_print_position(position):
    """Print the position."""
    print(str(position), "\n")


def ask_user_and_solve():
    """Ask user about problem setup and solve.

    Print the results to console afterwards
    """
    rows, cols, piece_set = user_input()
    print("Setup: ")
    for piece_klass in PIECES:
        cnt = piece_set.get(piece_klass)
        if cnt:
            print("  {}: {}".format(piece_klass.full_name(), cnt))
    print()

    total_solutions = 0

    for position in solve(rows, cols, piece_set):
        pretty_print_position(position)
        total_solutions += 1

    print("\nThis setup has {} solutions\n".format(total_solutions))


if __name__ == '__main__':
    ask_user_and_solve()
