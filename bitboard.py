"""module defining classes for board, position, pieces."""


from itertools import product, chain


class BitBoard(object):
    """class representing board as integer (bitstring).

    Instance of this class encapsulates operations with bitmask
    represented board of given size
    """

    def __init__(self, rows, cols):
        """Constructor.

        Args:
              rows, cols  -- size of the board
        """
        self.rows, self.cols = rows, cols
        self.first_row = (1 << self.cols) - 1

    @property
    def size_rows(self):
        """Return rows size of board."""
        return self.rows

    @property
    def size_cols(self):
        """Return columns size of board."""
        return self.cols

    def bit_by_coordinates(self, row, col):
        """Return integer with given coordinates bit set.

        Args:
              row, col  -- coordinates of position
        """
        if self.rows > row >= 0 and self.cols > col >= 0:
            return 1 << (self.cols * row) << col
        return 0

    def coordinates_by_bit(self, bit):
        """Return pair of coordinates for square.

        Square identified by higher bit set in int value
        Args:
              bit  -- integer value (see above)
        """
        row, col = 0, 0
        while bit > self.first_row:
            bit >>= self.cols
            row += 1
        while bit > 1:
            bit >>= 1
            col += 1

        return row, col

    def full_board(self):
        """Return integer with all bits set for the board."""
        return (1 << self.rows * self.cols) - 1

    def full_column(self, col):
        """Return integer with all bits on given column set."""
        cbit = 1 << col
        result = 0
        for _ in range(self.rows):
            result |= cbit
            cbit <<= self.cols
        return result

    def full_row(self, row):
        """Return integer with all bits on given row set."""
        return self.first_row << (row * self.cols)

    def slash(self, row, col):
        """Return integer with all bits on clockwise diagonal set."""
        result = 0
        _normalize = min(row, self.cols - col - 1)
        row -= _normalize
        col += _normalize
        _bit = self.bit_by_coordinates(row, col)
        while row < self.rows and col >= 0:
            result |= _bit
            _bit <<= (self.cols - 1)
            col -= 1
            row += 1
        return result

    def backslash(self, row, col):
        """Return integer with all bits on counter clockwise diagonal set."""
        result = 0
        _normalize = min(row, col)
        row -= _normalize
        col -= _normalize
        _bit = self.bit_by_coordinates(row, col)
        while row < self.rows and col < self.cols:
            result |= _bit
            _bit <<= (self.cols + 1)
            col += 1
            row += 1
        return result


class Piece(object):
    """Base class for chess piece objects."""

    _shortname_ = '?'
    _fullname_ = '?piece?'

    def __init__(self, board, row, col):
        """Constructor.

        Args:
              board     -- board where the piece is set
              row, col  -- piece coordinates
        """
        self.board = board
        self.row, self.col = row, col

    def _attacks(self):
        """Return attacked by the piece fields."""
        return NotImplementedError('This is abstract class {}'
                                   .format(self.__class__.__name__))

    def influence(self):
        """Return bitmask of influenced sqaures.

        Return bitmask off all the squares attacked by particular
        piece plus the square where the piece iself is accomodated
        """
        return self._attacks() | \
            self.occupied()

    def occupied(self):
        """Return bit for the occupied square."""
        return self.board.bit_by_coordinates(self.row, self.col)

    @classmethod
    def short_name(cls):
        """Return the short one-char abbreviation of piece."""
        return cls._shortname_

    @classmethod
    def full_name(cls):
        """Return the full name of piece."""
        return cls._fullname_


class Knight(Piece):
    """Knight."""

    _shortname_ = 'N'
    _fullname_ = 'Knight'

    def _attacks(self):
        """Return squares attacked by knight."""
        return self.board.bit_by_coordinates(self.row - 2, self.col - 1) | \
            self.board.bit_by_coordinates(self.row - 2, self.col + 1) | \
            self.board.bit_by_coordinates(self.row + 2, self.col - 1) | \
            self.board.bit_by_coordinates(self.row + 2, self.col + 1) | \
            self.board.bit_by_coordinates(self.row - 1, self.col - 2) | \
            self.board.bit_by_coordinates(self.row - 1, self.col + 2) | \
            self.board.bit_by_coordinates(self.row + 1, self.col - 2) | \
            self.board.bit_by_coordinates(self.row + 1, self.col + 2)


class Bishop(Piece):
    """Bishop."""

    _shortname_ = 'B'
    _fullname_ = 'Bishop'

    def influence(self):
        """Return Bishop influenced squares."""
        return self.board.slash(self.row, self.col) | \
            self.board.backslash(self.row, self.col)


class Rook(Piece):
    """Rook."""

    _shortname_ = 'R'
    _fullname_ = 'Rook'

    def influence(self):
        """Return Rook influenced squares."""
        return self.board.full_row(self.row) | \
            self.board.full_column(self.col)


class Queen(Piece):
    """Queen."""

    _shortname_ = 'Q'
    _fullname_ = 'Queen'

    def influence(self):
        """Return Queen influenced squares."""
        return self.board.slash(self.row, self.col) | \
            self.board.backslash(self.row, self.col) | \
            self.board.full_row(self.row) | \
            self.board.full_column(self.col)


class King(Piece):
    """King."""

    _shortname_ = 'K'
    _fullname_ = 'King'

    def _attacks(self):
        """Return squares attacked by King."""
        return self.board.bit_by_coordinates(self.row - 1, self.col - 1) | \
            self.board.bit_by_coordinates(self.row - 1, self.col + 1) | \
            self.board.bit_by_coordinates(self.row - 1, self.col) | \
            self.board.bit_by_coordinates(self.row, self.col - 1) | \
            self.board.bit_by_coordinates(self.row, self.col + 1) | \
            self.board.bit_by_coordinates(self.row + 1, self.col - 1) | \
            self.board.bit_by_coordinates(self.row + 1, self.col + 1) | \
            self.board.bit_by_coordinates(self.row + 1, self.col)


class Position(object):
    """Incapsulate the position state: board, pieces, influenced squares."""

    def __init__(self, board, influence=None, occupied=None, position=None):
        """Constructor.

        if you don't pass actual influence and occupied bitmasks
        it will get them from the actual position
        """
        self.board = board
        self.position = position or []
        self.occupied = occupied or self.get_occupied_bits()
        self.influence = influence or self.get_influence_bits()

    def get_occupied_bits(self):
        """Return the bitwise or of the sqaures occupied by the pieces."""
        result = 0
        for piece in self.position:
            result |= piece.occupied()
        return result

    def get_influence_bits(self):
        """Return the bitwise or of the squares attacked and occupied."""
        result = 0
        for piece in self.position:
            result |= piece.influence()
        return result

    def try_accomodate_piece(self, piece):
        """Try to accomodate piece the way it goes to free non-attacked place.

        Return True and add piece to position if it's possible, else False
        """
        piece_occupied = piece.occupied()
        if self.influence & piece_occupied:
            return False

        piece_influence = piece.influence()
        if self.occupied & piece_influence:
            return False

        self.influence |= piece_influence
        self.occupied |= piece_occupied
        self.position.append(piece)
        return True

    def clone(self):
        """Return copy of the object."""
        return self.__class__(self.board, self.influence, self.occupied,
                              self.position[:])

    def __str__(self):
        """Printable representation of the position."""
        array_board = [['.'] * self.board.size_cols
                       for _ in range(self.board.size_rows)]

        for piece in self.position:
            array_board[piece.row][piece.col] = piece.short_name()

        return "\n".join("".join(s) for s in array_board)


if __name__ == '__main__':
    import unittest

    class TestBitBoard(unittest.TestCase):
        """test BitBoard."""

        def setUp(self):
            """set up."""
            self.board1x1 = BitBoard(1, 1)
            self.board2x2 = BitBoard(2, 2)
            self.board3x3 = BitBoard(3, 3)
            self.board2x10 = BitBoard(2, 10)
            self.board10x2 = BitBoard(10, 2)
            self.board10x10 = BitBoard(10, 10)
            self.boards = (
                self.board1x1, self.board2x2, self.board3x3,
                self.board2x10, self.board10x2, self.board10x10
            )

        def test_full_board(self):
            """test BitBoard.full_board."""
            self.assertEqual(self.board1x1.full_board(), 1)
            self.assertEqual(self.board2x2.full_board(), 0b1111)
            self.assertEqual(self.board3x3.full_board(), 0b111111111)
            self.assertEqual(self.board2x10.full_board(), (1 << 20) - 1)
            self.assertEqual(self.board10x2.full_board(), (1 << 20) - 1)
            self.assertEqual(self.board10x10.full_board(), (1 << 100) - 1)

        def test_bit_by_coordinates(self):
            """test BitBoard.bit_by_coordinates."""
            self.assertEqual(self.board1x1.bit_by_coordinates(0, 0), 1)
            self.assertEqual(self.board2x2.bit_by_coordinates(0, 0), 1)
            self.assertEqual(self.board10x10.bit_by_coordinates(0, 0), 1)
            self.assertEqual(self.board2x2.bit_by_coordinates(1, 1), 0b1000)
            self.assertEqual(self.board3x3.bit_by_coordinates(1, 1), 0b10000)
            self.assertEqual(
                self.board3x3.bit_by_coordinates(2, 2), 0b100000000)
            self.assertEqual(self.board2x10.bit_by_coordinates(1, 8), 1 << 18)
            self.assertEqual(self.board10x2.bit_by_coordinates(8, 1), 1 << 17)
            self.assertEqual(self.board10x10.bit_by_coordinates(5, 5), 1 << 55)

        def test_coordinates_by_bit(self):
            """test BitBoard.coordinates_by_bit."""
            self.assertEqual(self.board1x1.coordinates_by_bit(1), (0, 0))
            self.assertEqual(self.board2x2.coordinates_by_bit(1), (0, 0))
            self.assertEqual(self.board10x10.coordinates_by_bit(1), (0, 0))
            self.assertEqual(self.board2x2.coordinates_by_bit(0b1000), (1, 1))
            self.assertEqual(self.board3x3.coordinates_by_bit(0b10000), (1, 1))
            self.assertEqual(
                self.board3x3.coordinates_by_bit(0b100000000), (2, 2))
            self.assertEqual(
                self.board2x10.coordinates_by_bit(1 << 18), (1, 8))
            self.assertEqual(
                self.board10x2.coordinates_by_bit(1 << 17), (8, 1))
            self.assertEqual(
                self.board10x10.coordinates_by_bit(1 << 55), (5, 5))

        def test_full_row(self):
            """test BitBoard.full_row."""
            for bitbrd in self.boards:
                for row in range(bitbrd.size_rows):
                    full_row = bitbrd.full_row(row)
                    for col in range(bitbrd.size_cols):
                        check_bit = bitbrd.bit_by_coordinates(row, col)
                        self.assertTrue(full_row & check_bit)
                        full_row ^= check_bit
                    self.assertFalse(full_row)

        def test_full_column(self):
            """test BitBoard.full_column."""
            for bitbrd in self.boards:
                for col in range(bitbrd.size_cols):
                    full_column = bitbrd.full_column(col)
                    for row in range(bitbrd.size_rows):
                        check_bit = bitbrd.bit_by_coordinates(row, col)
                        self.assertTrue(full_column & check_bit)
                        full_column ^= check_bit  # reset bit after check
                    self.assertFalse(full_column)  # no extra bits

        def test_slash(self):
            """test BitBoard.slash."""
            for bitbrd in self.boards:
                for row, col in product(range(bitbrd.size_rows),
                                        range(bitbrd.size_cols)):
                    slash = bitbrd.slash(row, col)
                    # check bits in two slash directions from the point
                    for row1 in chain(range(row + 1, bitbrd.size_rows),
                                      range(row - 1, -1, -1)):
                        col1 = col - (row1 - row)
                        if col1 >= 0 and col1 < bitbrd.size_cols:
                            check_bit = bitbrd.bit_by_coordinates(row1, col1)
                            self.assertTrue(slash & check_bit)
                            slash ^= check_bit
                    self.assertEqual(slash,
                                     bitbrd.bit_by_coordinates(row, col))

        def test_backslash(self):
            """test BitBoard.backslash."""
            for bitbrd in self.boards:
                for row, col in product(range(bitbrd.size_rows),
                                        range(bitbrd.size_cols)):
                    backslash = bitbrd.backslash(row, col)
                    # check bits in two backslash directions from the point
                    for row1 in chain(range(row + 1, bitbrd.size_rows),
                                      range(row - 1, -1, -1)):
                        col1 = col + (row1 - row)
                        if col1 >= 0 and col1 < bitbrd.size_cols:
                            check_bit = bitbrd.bit_by_coordinates(row1, col1)
                            self.assertTrue(backslash & check_bit)
                            backslash ^= check_bit
                    self.assertEqual(backslash,
                                     bitbrd.bit_by_coordinates(row, col))

    class TestPosition(unittest.TestCase):
        """test Position class."""

        def setUp(self):
            """set up positions for tests."""
            self.board3x3 = BitBoard(3, 3)
            self.board5x5 = BitBoard(5, 5)
            self.position3x3 = Position(self.board3x3)
            self.position5x5 = Position(self.board5x5)

        def test_try_accomodate_piece(self):
            """test Position.try_accomodate_piece."""
            self.assertTrue(self.position3x3.try_accomodate_piece(
                Queen(self.board3x3, 1, 1)))
            self.assertFalse(self.position3x3.try_accomodate_piece(
                Queen(self.board3x3, 1, 1)))
            self.assertFalse(self.position3x3.try_accomodate_piece(
                Queen(self.board3x3, 0, 0)))
            self.assertEqual(self.board3x3.bit_by_coordinates(1, 1),
                             self.position3x3.occupied)
            self.assertEqual(self.board3x3.full_board(),
                             self.position3x3.influence)

        #  TODO more tests

    unittest.main()
