"""Main module for the chess pieces allocation problem solver."""

from itertools import permutations, product
from bitboard import BitBoard, King, Knight, Bishop, Rook, Queen, Position


def possible_layouts(position, piece_klass, piece_count):
    """Heart of the evaluation.

    Iterator:

    Given some position and a count of pieces to accomodate tries to put
    the number of pieces on board not on attacked or occupied squares
    """
    if piece_count == 0:  # we have no pieces to try, just yield back position
        yield position
        return

    board = position.board

    #  product(range(...), range(...)) will produce unique tuples of board
    #  coordinates
    #
    #  permutations(product(...), C) will produce the possible
    #  combinations (tuples of length C) of coordinates for C pieces
    #
    #  the all(...) will filter out the combinations where the coordinates
    #  are not in sorted order. since we are trying to accomodate
    #  similar pieces we'll get same setups with different ordering and this
    #  predictively filters out those
    #

    for new_pieces_coords in (c for c in permutations(
            ((row, col) for row, col
             in product(range(board.size_rows), range(board.size_cols))
             if not position.influence & board.bit_by_coordinates(row, col)),
            piece_count) if all(c[i] < c[i+1] for i in range(piece_count-1))):

        new_position = position.clone()

        for row, col in new_pieces_coords:
            piece = piece_klass(board, row, col)
            if not new_position.try_accomodate_piece(piece):
                break

        else:
            yield new_position


def solve(rows, cols, piece_set):
    """Solver.

    Solve the task of allocating the chess pieces
    in a way that every piece doesn't attack each other
    """
    board = BitBoard(rows, cols)

    return (position

            for position_q in
            possible_layouts(Position(board), Queen, piece_set.get(Queen, 0))

            for position_r in
            possible_layouts(position_q, Rook, piece_set.get(Rook, 0))

            for position_b in
            possible_layouts(position_r, Bishop, piece_set.get(Bishop, 0))

            for position_n in
            possible_layouts(position_b, Knight, piece_set.get(Knight, 0))

            for position in
            possible_layouts(position_n, King, piece_set.get(King, 0)))


if __name__ == '__main__':
    import unittest

    class TestSolution(unittest.TestCase):
        """tests for the solver."""

        def test_solution(self):  # this is very basic tests now TODO extend
            """some basic tests for now."""
            self.assertEqual(len(list(solve(3, 3, {King: 1}))), 3 * 3)
            self.assertEqual(len(list(solve(3, 3, {Queen: 1}))), 3 * 3)
            self.assertEqual(len(list(solve(2, 5, {King: 1}))), 2 * 5)

        #  TODO more tests

    unittest.main()
